import {  notification } from 'antd';

export default function useNotification() {
    function alertShow(type, text) {
        switch (type) {
          case "success":
            return notification.success({
                message : text
            });
          case "error":
            return notification.error({
                message : text
            });
          default:
            return null;
        }
      }
    return {
       alertShow
    }
}
