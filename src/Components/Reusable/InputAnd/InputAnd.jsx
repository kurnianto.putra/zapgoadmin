import React from "react";
import Style from "./inputantd.module.scss";

function StyleInput({ label,
    placeholder,
    key,
    onChange,
    value,
    name,
    disabled
}) {
    return (
        <div className={Style["register"]}>
            <form>
                <label htmlFor={key}>
                    <input disabled={disabled} style={disabled ? { background: "#f0f0f0", cursor: "not-allowed" } : undefined} onChange={onChange} value={value} name={name} id={key} placeholder={placeholder} />
                    <span>{label}</span>
                </label>
            </form>
        </div>
    )
}
export default StyleInput
