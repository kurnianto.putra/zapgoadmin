import React from 'react';
import { Button, Modal, Tooltip } from 'antd';
import Styles from "./Modal.module.css"
import { CloseOutlined  } from '@ant-design/icons'

function ModalZapgo({title, children , showModal = false , handleCancel, ...props  }){
  return (
      <Modal closable={false} title={null} visible={showModal} onCancel={handleCancel}
      footer={[]} {...props}     maskClosable = {false}
      >
        <div className={Styles.wrap}>
          <div className={Styles.judul}>{title}</div>
          <div className={Styles.exit}>
            <Tooltip title="Keluar">
              <Button shape="circle" onClick={handleCancel} icon={<CloseOutlined style={{color:"rgb(165, 3, 3)"}} />} size="large" />
            </Tooltip>
          </div>
        </div>
        {children}
      </Modal>
  );
};
export default ModalZapgo