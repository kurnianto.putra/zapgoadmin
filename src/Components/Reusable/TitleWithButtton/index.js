import React from 'react'
import Style from "./TitleWithButton.module.css"
import { Button } from 'antd'
import { PlusCircleOutlined } from '@ant-design/icons'

export default function TitleWithButton({title,children, isButton , onClick}) {
    return (
        <div className={Style.headerwrap}>
            <div className={Style.header}>
            {title}
            </div>
            <div className={Style.Button}>
                {children}
                {isButton ?
                <Button onClick={onClick} className={Style["buttontit"]} icon={<PlusCircleOutlined />} >
                    Tambah Data
                </Button>
                :null    
                }
            </div>
        </div>
    )
}
