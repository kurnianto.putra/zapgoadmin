import React from 'react'
import { Layout, Modal , Button } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import Style from "./content.module.css"
import { getCurrentDate } from '../../../Utils';
import Icon,{
  UserOutlined,
  LogoutOutlined
} from '@ant-design/icons';
import {AuthContext} from "../../../Utils/context"
const { Header,  Content } = Layout;
export default function ContentAndHeader({children , nama }) {
  const { setLoggedIn } = React.useContext(AuthContext);
  const { confirm } = Modal;

  const logout = () => {
    confirm({
      title: "Apakah Anda yakin untuk keluar?",
      icon: <ExclamationCircleOutlined />,
      content: "Klik OK untuk keluar",
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0 ? resolve : reject, 200);
          localStorage.removeItem("toksen");
          localStorage.removeItem("user");
          setLoggedIn(false);
        });
      },
      onCancel() {},
    });
  };
    return (
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <div className={Style.header}>
              <div className={Style.time}>{getCurrentDate()}</div>
              <div className={Style.users}>
                <div>
                  <Icon component={UserOutlined} style={{fontSize:"20px"}} /> Selamat Datang , <span>{localStorage.getItem("user")}</span>
                </div>
                <div className={Style.logut}>
                  <Button onClick={()=> logout()}  ghost icon={<LogoutOutlined style={{color:"#5c0011" , fontWeight:600}} />}/>
                </div>
                </div>
              </div>
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            {children}
          </Content>
        </Layout>
    )
}
