import React ,{useState} from 'react'
import { Link, useLocation } from "react-router-dom";
import { Layout, Menu } from 'antd';
import { Button } from 'antd';
import {
  UserOutlined,
  ReadOutlined,
  FlagOutlined,
  FileImageOutlined,
  DoubleLeftOutlined,
  DoubleRightOutlined,
  BulbOutlined
} from '@ant-design/icons';
const { Sider } = Layout;
export default function SideBar() {
    const location = useLocation();
    const [collapsed , setCollapsed] = useState(false)


    const toggle = () => {
      setCollapsed(!collapsed)
    };
    function ButtonSide(){
        return (
            <Button onClick={toggle} style={{width:"100%" , backgroundColor:"transparent" , border :"none"}}>
                {collapsed ? <DoubleRightOutlined /> : <DoubleLeftOutlined/>}
            </Button>
        )
    }
    return (
             <Sider trigger={ButtonSide()} collapsible collapsed={collapsed}>
                    <div className="logo">ZAPGO</div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} selectedKeys={[location.pathname]}>
                    <Menu.Item key="/customer" icon={<UserOutlined />}>
                         <Link to="/customer">  List Customer</Link>
                    </Menu.Item>
                    <Menu.Item key="/artikel" icon={<ReadOutlined />}>
                        <Link to="/artikel">Master Artikel</Link>
                    </Menu.Item>
                    <Menu.Item key="/negara" icon={<FlagOutlined />}>
                    <Link to="/negara">Master Negara</Link>
                    </Menu.Item>
                    {/* <Menu.Item key="/keunggulan" icon={<ShoppingCartOutlined />}>
                        <Link to="/keunggulan">Master Keunggulan</Link>
                    </Menu.Item>
                    <Menu.Item key="/proses-pengiriman" icon={<CarOutlined />}>
                        <Link to="/proses-pengiriman">Proses Pengiriman</Link>
                    </Menu.Item> */}
                    <Menu.Item key="/sliderpromosi" icon={<BulbOutlined />}>
                        <Link to="/sliderpromosi">Gambar Promosi </Link>
                    </Menu.Item>
                    <Menu.Item key="/banner" icon={<FileImageOutlined />}>
                        <Link to="/banner">Master Background</Link>
                    </Menu.Item>
                </Menu>
        </Sider>
    )
}
