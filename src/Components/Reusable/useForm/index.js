import { useState } from "react";

const useForm = (callback) => {
  const [values, setValues] = useState({});

  const handleSubmit = (event) => {
    if (event) event.preventDefault();
    callback();
  };

  const deleteForm = () => {
    Array.from(
      document.querySelectorAll("input,textarea,select,input[type=radio]")
    ).forEach((input) => (input.value = ""));
    setValues(Object.assign({}, {}));
  };

  const handleChange = (event) => {
    event.persist();
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };
  return {
    handleChange,
    handleSubmit,
    values,
    setValues,
    deleteForm,
  };
};

export default useForm;
