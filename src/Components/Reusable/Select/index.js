import React, { useEffect, useState } from "react";
import { Select } from 'antd';
import Style from "./Select.module.scss"
const { Option } = Select;
const CustomSelect = ({  handleChange }) => {
	return (
		<div className={`${Style.CustomSelect}`}>
	
            <Select
                    labelInValue
                    defaultValue={{ value: '' }}
                    style={{ width: 120 }}
                    onChange={handleChange}
                >
                    <Option value="" disabled>Cari Berdasarkan</Option>
                    <Option value="jenis_barang">Jenis Barang</Option>
                    <Option value="no_hp">No HP</Option>
                    <Option value="nama_user">Nama</Option>
                </Select>
		</div>
	);
};

export default CustomSelect;
