import React from 'react'
import "./styleinput.css"
import { Input } from 'antd'
function StyleInput({placeholder, ...props}) {
    return (
        <div class="form-group">
            <Input {...props} className="form-style" placeholder={placeholder} id="logemail" autocomplete="off"/>
        </div>	
    )
}
export default StyleInput