import Style from "../Customer.module.css"
import "./Column.css"
import Icon , {DeleteOutlined , EditOutlined , UploadOutlined} from "@ant-design/icons"
import {Divider, Button } from "antd"

import { Image } from 'antd';
const Columns = ({hapus , tambahFoto, editData, hapusFoto}) =>[
  {
    title: 'Tanggal Buat',
    dataIndex: 'created_at',
    key: 'created_at',
    render: text => <div className={Style.text}>{new Date(text).toLocaleDateString("id-ID")}</div>,

  },
  {
    title: 'Judul Artikel',
    dataIndex: 'judul_artikel',
    key: 'judul_artikel',
    render: text => <div className={Style.text}>{text}</div>,
  },
  {
    title: 'Action',
    dataIndex: 'action',
    render: (_,record) => {
      if(record?.gambar.length >= 1){
        return <div className="wrapbuttonaksi"> 
          <div className="boxsatu">
            <Image src={record.gambar} width={50} height={50}/>
          </div>
          <div className="boxsatu">
           <Button onClick={()=> hapusFoto(record)} type="primary" shape="circle" icon={<DeleteOutlined/>}/>
         </div>
         </div>
      }
      return  <Button type="primary" onClick={() => tambahFoto(record)} icon={<UploadOutlined />} size={"large"}>
      Upload
    </Button>
  }
},
  {
    title: 'Keterangan',
    dataIndex: 'deskripsi',
    key: 'deskripsi',
    render: text => <div className={Style.desk}>{text}</div>,

  },
  {
    title: 'Total Dilihat',
    dataIndex: 'view_count',
    key: 'view_count',
    render: text => <div className={Style.text}>{text}</div>,

  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: (_, record) => {
        return (
          <span style={{display:'flex'}}>
            <div style={{ width:"50px" ,  height:"50px", display:"flex" , cursor:"pointer"}}>
              <Icon onClick={()=> hapus(record)} component={DeleteOutlined} style={{ margin:"auto" , fontSize: '19px' , cursor:"pointer" }}/>
            </div>
            <Divider type="vertical" />
            <div style={{ width:"50px" ,  height:"50px", display:"flex", cursor:"pointer"}}>
            <Icon onClick={()=> editData(record)} component={EditOutlined} style={{margin:"auto", fontSize: '19px' , cursor:"pointer" }}/>
            </div>
          </span>
        )
    }
  }
];


export default Columns;