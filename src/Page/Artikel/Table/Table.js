import { Table } from 'antd'
import React from 'react'
import Columns from "./Column"
export default function TableCustomer({data , hapus , tambahFoto, editData, hapusFoto}) {
    return (
        <Table
            dataSource={data}
            columns={Columns({hapus , tambahFoto, editData, hapusFoto})}
            pagination={{pageSize : 5}}
        />
    )
}
