import React from 'react'
import Input from '../../../Components/Reusable/Input'
import Style from "../../../Components/Reusable/TitleWithButtton/TitleWithButton.module.css"
import {Button} from "antd"
function TambahFoto({handleCancel, simpanFoto, handleFoto}) {
    return (
   <>
          <Input 
           type="file"
              panjang={"200"}
              name="nama_artikel"
              onChange={handleFoto}
           />
         
          <Button key="back"
          className={Style["buttonclose"]}
          onClick={handleCancel}>
          Return
        </Button>,
        <Button
          className={Style["buttonsave"]}
          key="submit" type="primary" onClick={simpanFoto}>
          Submit
        </Button>
    </>
    )
}
export default TambahFoto