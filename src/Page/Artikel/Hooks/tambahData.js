import React from 'react'
import Input from '../../../Components/Reusable/Input'
import InputAnd from '../../../Components/Reusable/InputAnd'
import Style from "../../../Components/Reusable/TitleWithButtton/TitleWithButton.module.css"
import {Button} from "antd"
import { Editor } from "@tinymce/tinymce-react";
function TambahBannerModal({handleCancel, mode, onCreate , onUpdate , values, ubahValue, onChangeArtikel}) {
        const simpanData = (e) => {
          e.preventDefault();
          mode === "create" ? onCreate() : onUpdate();
         }
         console.log("isi artikel",values.isi_artikel)
    return (
   <>
          <InputAnd 
          key={"judul_artikel"}
          label={"Judul Artikel"}
          placeholder={"Judul Artikel"}
              name="judul_artikel"
              value={values.judul_artikel || ""}
              onChange={ubahValue}
           />
           <InputAnd 
          key={"deskripsi"}
          label={"Deskripsi Artikel"}
          placeholder={"Deskripsi Artikel"}
          name="deskripsi"
            value={values.deskripsi || ""}
            onChange={ubahValue}
          />
          <InputAnd 
            key={"keyword"}
            label={"Keyword Artikel"}
            placeholder={"Keyword Artikel"}
            name="keyword"
            value={values.keyword || ""}
            onChange={ubahValue}

          />
           <InputAnd 
           
            key={"kreator"}
            label={"Kreator Artikel"}
            placeholder={"Kreator Artikel"}
            name="creator"
            value={values.creator || ""}
            onChange={ubahValue}
            placeholder={"Creator"}
          />
            <InputAnd 
              key={"Alt"}
              label={"Alt Gambar"}
              placeholder={"Alt Gambar"}
            name="alt_text"
            value={values.alt_text || ""}
            onChange={ubahValue}
          />
          <Editor
                    init={{
                      plugins: "code advcode lists advlist",
                      toolbar:
                        "undo redo | bold italic| numlist bullist | alignleft aligncenter alignright | code ",
                      width: 1010,
                      height: 435,
                      font_formats: "Nunito=nunito",
                      fontsize_formats: "12pt 14pt 18pt 24pt 36pt",
                      content_style:
                        "@import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap');" +
                        "body { font-family: Nunito; };",
                    }}
                    name="isi_artikel"
                    onEditorChange={onChangeArtikel}
                    value={values.isi_artikel}
                    
                  />
          <Button key="back"
          className={Style["buttonclose"]}
          onClick={handleCancel}>
          Return
        </Button>,
        <Button
          className={Style["buttonsave"]}
          key="submit" type="primary" onClick={(e) =>simpanData(e)}>
          Submit
        </Button>
    </>
    )
}
export default TambahBannerModal