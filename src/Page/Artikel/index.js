import React,{useState , useEffect} from 'react'
import ContentAndHeader from '../../Components/Reusable/Content'
import TableCustomer from './Table/Table'
import TitleWithButton from '../../Components/Reusable/TitleWithButtton'
import { Modal,  Input} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import useNotification from '../../Components/Reusable/Alert'
import TambahBannerModal from './Hooks/tambahData'
import TambahFoto from './Hooks/tambahFoto'
import useForm from '../../Components/Reusable/useForm'
import ModalZapgo from '../../Components/Reusable/Modal'
import { listArtikel , tambahArtikel , deleteArtikel, editDataArtikel } from './Method'
import { deleteGambar , uploadGambar } from '../Promosi/Method';
const { Search } = Input;
const { confirm } = Modal;
export default function SlidePromosi() {
  const { values, handleChange, handleSubmit, setValues } =useForm(formBanner);
  const [view, setView] = useState(false)
  const [mode,setMode] = useState("create")
  const [counter, setCounter] = useState(0)
  const [dataBanner , setDataBanner] = useState([])
  const [viewUpload, setViewUpload] = useState(false)
  const [key, setSearchKey] = useState("")
  const [idArtikel , setIdArtikel] = useState("")
  const [initial , setInitialData] = useState("")
  const [file, setFile] = useState("")
  const {alertShow} = useNotification()

  const getDataArtikel = (key) => listArtikel(1,key,"").then(({response})=> setDataBanner(response)) 
  
  useEffect(()=>{
    let isFecth = true 

    if(isFecth){
      getDataArtikel(key)
    }
  },[counter, key])
  function actionDelete(data){
     confirm({
       title: 'Apakah Anda yakin untuk menghapus?',
       icon: <ExclamationCircleOutlined />,
       content: `Menekan tombol "Ya" berarti anda bersedia untuk menghapus data banner ${data.judul} secara permanen`,
       okText: 'Yes',
       okType: 'danger',
       cancelText: 'No',
       onOk() {
         deleteArtikel(data.uid)
         .then(()=> setCounter((count)=> count+1))
         .then(()=> alertShow("success",`Berhasil menghapus banner ${data.judul}` ))
         .catch(()=> alertShow("error",`Gagal menghapus banner ${data.judul}` ))
       },
       onCancel() {
         console.log('Cancel');
       },
     });
  }

  function formBanner(){
    const data = {
      judul_artikel : values.judul_artikel ,
      deskripsi : values.deskripsi,
      creator : values.creator ,
      keyword : values.keyword,
      alt_text : values.alt_text,
      isi_artikel : values.isi_artikel
    }
    tambahArtikel(data)
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil menyimpan artikel ${data.judul_artikel}`)
      setValues({})
      setView(false)
    }
    )
    .catch(()=> alertShow("error",`Gagal menyimpan artikel ${data.judul_artikel}` ))
  }

  useEffect(() => {
    if (idArtikel) {
      const GetData = async () => {
        const {response} = await listArtikel(1,"",idArtikel);
        setValues({
          uid: response[0].uid,
          judul_artikel: response[0].judul_artikel,
          deskripsi: response[0].deskripsi,
          keyword: response[0].keyword,
          created_at: response[0].created_at,
          view_count: response[0].view_count,
          isi_artikel : response[0].isi_artikel,
          alt_text : response[0].alt_text
        });
        setView(true)
        setMode("edit")
      };
      GetData();
    }
  }, [idArtikel]);
  function actiontambah(record){
    setIdArtikel(record.uid)
  }

  function resetData(){
    setValues({})
    setView(false)
    setMode("create")
  }

  function handleUpdate(){
     const data = {
      judul_artikel : values.judul_artikel ,
      deskripsi : values.deskripsi,
      creator : values.creator ,
      keyword : values.keyword,
      isi_artikel : values.isi_artikel,
      alt_text : values.alt_text
    }
    editDataArtikel(idArtikel,data)
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil memperbarui artikel ${data.judul_artikel}`)
      setValues({})
      setView(false)
    }
    )
    .catch(()=> alertShow("error",`Gagal memperbarui artikel ${data.judul_artikel}` ))
  }

  function viewTambahFoto(record){
    setViewUpload(true)
    setValues({
      keterangan: record.keterangan,
      judul: record.judul,
      uid: record.uid
    })
  }
  function tambahFoto(){
    const form = new FormData();
    form.append("uid_gambar", values.uid);
    form.append("nama_gambar", file );
    form.append("kategori", 'artikel');
    uploadGambar(form)         
    .then(()=>{
      setCounter(counter+1)
      alertShow("success",`Berhasil  menambahkan gambar }`)
      setValues({})
      setViewUpload(false)
    })
    .catch(()=> alertShow("error",`Gagal menambahkan gambar  ${values.judul}` ))
  }

  function hapusImage(rec){
    deleteGambar(rec.uid)
    .then(()=>{
      setCounter(counter+1)
      alertShow("success",`Berhasil  mengahpus gambar `)
      setValues({})
      setViewUpload(false)
    })
    .catch(()=> alertShow("error",`Gagal mengahpus gambar ` ))
  }


  return (
      <ContentAndHeader>
          <ModalZapgo 
            showModal={view}
            width={"80%"}
        
              handleCancel={()=> setView(false)}
            title={mode === "create" ? "Modal Tambah Data Artikel" : "Modal Edit Data Artikel"}
        >
           <TambahBannerModal 
           values={values} 
           ubahValue={handleChange}
           onCreate={handleSubmit}
           onUpdate={handleUpdate}
           handleCancel={resetData}
           onChangeArtikel={(content) => setValues((artikel)=> ({...artikel , isi_artikel : content})) }
           mode={mode}
           />
        </ModalZapgo>
        <ModalZapgo 
            showModal={viewUpload}
            handleCancel={()=> setViewUpload(false)}
            title={"Modal Tambah Gambar Artikel"}
        >

        <TambahFoto simpanFoto={tambahFoto} handleFoto={(e) => setFile(e.target.files[0])} /> 
        </ModalZapgo>
      <TitleWithButton 
      handle
      onClick={() => setView(true)} isButton={true} title={"Master Data Artikel"}>
      <Search 
        placeholder="Cari" 
        allowClear 
        onChange={(e)=> setInitialData(e.target.value)}
        onSearch={() => setSearchKey(initial)} 
        size="large"
        style={{ width: 200 ,padding:"10px 5px" }} />
      </TitleWithButton>
          <div><TableCustomer
          editData={(record) => actiontambah(record)}
          hapus={(record)=>actionDelete(record)} hapusFoto={(record)=> hapusImage(record)} data={dataBanner && dataBanner}
          tambahFoto={(record)=> viewTambahFoto(record)}
          /></div>
      </ContentAndHeader>
    )
}
