import {requestOnline} from "../../../Utils/request"

export const tambahArtikel = (data) => requestOnline("POST", `/artikel` ,data)
export const listArtikel = (page=1, cari = "", uid) => requestOnline("GET", `/artikel?page=${page}&cari=${cari}&uid=${uid}`)
export const deleteArtikel = id => requestOnline("DELETE" , `/artikel/${id}`)
// export const editData = (id, data) = requestOnline("PUT", `/artikel/${id}` , data)
export const editDataArtikel = (id,data) => requestOnline("PUT", `/artikel/${id}` ,data)
