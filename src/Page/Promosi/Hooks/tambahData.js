import React from 'react'
import Style from "../../../Components/Reusable/TitleWithButtton/TitleWithButton.module.css"
import {Button} from "antd"
import InputAnd from '../../../Components/Reusable/InputAnd'
function TambahBannerModal({handleCancel, mode, onCreate , onUpdate , values, ubahValue}) {
        const simpanData = (e) => {
          e.preventDefault();
          mode === "create" ? onCreate() : onUpdate();
         }
    return (
   <>
          <InputAnd 
              type="text"
              panjang={"200"}
              placeholder="Nama Banner"
              name="nama_banner"
              value={values.nama_banner || ""}
              onChange={ubahValue}
              key={"nama_banner"}
              label={"Nama Banner"}

           
           />
          <InputAnd 
            name="keterangan"
            value={values.keterangan || ""}
            onChange={ubahValue}
          placeholder={"Deskripsi"}
          disabled={mode === "edit"}
          key={"keterangan"}
          label={"Keterangan"}
          />
           <InputAnd 
            name="alt_text"
            value={values.alt_text || ""}
            onChange={ubahValue}
            placeholder={"Alt Text"}
            key={"alt_text"}
            label={"Alt Text"}
            />
          <Button key="back"
          className={Style["buttonclose"]}
          onClick={handleCancel}>
          Return
        </Button>,
        <Button
          className={Style["buttonsave"]}
          key="submit" type="primary" onClick={(e) =>simpanData(e)}>
          Submit
        </Button>
    </>
    )
}
export default TambahBannerModal