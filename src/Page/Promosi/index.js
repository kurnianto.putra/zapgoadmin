import React,{useState , useEffect} from 'react'
import ContentAndHeader from '../../Components/Reusable/Content'
import TableCustomer from './Table/Table'
import TitleWithButton from '../../Components/Reusable/TitleWithButtton'
import { Modal,  Input} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import useNotification from '../../Components/Reusable/Alert'
import TambahBannerModal from './Hooks/tambahData'
import TambahFoto from './Hooks/tambahFoto'
import useForm from '../../Components/Reusable/useForm'
import ModalZapgo from '../../Components/Reusable/Modal'

import { deleteBanner, deleteGambar, listBanner, tambahBanner, updateBanner, uploadGambar } from './Method'
const { Search } = Input;
const { confirm } = Modal;
export default function SlidePromosi() {
  const { values, handleChange, handleSubmit, setValues } =useForm(formBanner);
  const [view, setView] = useState(false)
  const [mode,setMode] = useState("create")
  const [counter, setCounter] = useState(0)
  const [dataBanner , setDataBanner] = useState([])
  const [viewUpload, setViewUpload] = useState(false)
  const [key, setSearchKey] = useState("")
  const [initial , setInitialData] = useState("")
  const [file, setFile] = useState("")
  const {alertShow} = useNotification()

  const getDataCust = (key) => listBanner(5,0,key).then(({response})=> setDataBanner(response)) 
  
  useEffect(()=>{
    let isFecth = true 

    if(isFecth){
      getDataCust(key)
    }
  },[counter, key])
  function actionDelete(data){
     confirm({
       title: 'Apakah Anda yakin untuk menghapus?',
       icon: <ExclamationCircleOutlined />,
       content: `Menekan tombol "Ya" berarti anda bersedia untuk menghapus data banner ${data.nama_banner} secara permanen`,
       okText: 'Yes',
       okType: 'danger',
       cancelText: 'No',
       onOk() {
         deleteBanner(data.uid)
         .then(()=> setCounter((count)=> count+1))
         .then(()=> alertShow("success",`Berhasil menghapus banner ${data.nama_banner}` ))
         .catch(()=> alertShow("error",`Gagal menghapus banner ${data.nama_banner}` ))
       },
       onCancel() {
         console.log('Cancel');
       },
     });
  }

  function formBanner(){
    const data = {
      nama_banner : values.nama_banner ,
      keterangan : values.keterangan,
      alt_text : values.alt_text,
    }
    tambahBanner(data)
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil menyimpan banner ${data.nama_banner}`)
      setValues({})
      setView(false)
    }
    )
    .catch(()=> alertShow("error",`Gagal menyimpan banner ${data.nama_banner}` ))
  }


  function actiontambah(record){
    console.log("rec", record)
    setView(true)
      setMode("edit");
      setValues({
        keterangan: record.keterangan,
        nama_banner: record.nama_banner,
        alt_text : record.alt_text,
        uid: record.uid
      })
  }

  function resetData(){
    setValues({})
    setView(false)
    setMode("create")
  }

  function handleUpdate(){
    const data = {
      nama_banner : values.nama_banner,
      alt_text : values.alt_text,

    }
    updateBanner(values.uid, data ) 
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil Update banner ${data.nama_banner}`)
      setValues({})
      setView(false)
      setMode("create")
    }
    )
    .catch(()=> alertShow("error",`Gagal Update banner ${data.nama_banner}` ))
  }

  function viewTambahFoto(record){
    setViewUpload(true)
    setValues({
      keterangan: record.keterangan,
      nama_banner: record.nama_banner,
      uid: record.uid
    })
  }
  function tambahFoto(){
    const form = new FormData();
    form.append("uid_gambar", values.uid);
    form.append("nama_gambar", file );
    form.append("kategori", values.nama_banner);
    uploadGambar(form)         
    .then(()=>{
      setCounter(counter+1)
      alertShow("success",`Berhasil  menambahkan gambar banner ${values.nama_banner}`)
      setValues({})
      setViewUpload(false)
    })
    .catch(()=> alertShow("error",`Gagal menambahkan gambar banner ${values.nama_banner}` ))
  }

  function hapusImage(rec){
    deleteGambar(rec.uid)
    .then(()=>{
      setCounter(counter+1)
      alertShow("success",`Berhasil  mengahpus gambar banner`)
      setValues({})
      setViewUpload(false)
    })
    .catch(()=> alertShow("error",`Gagal mengahpus gambar banner` ))
  }

 


  return (
      <ContentAndHeader>
          <ModalZapgo 
            showModal={view}
            handleCancel={()=> setView(false)}
            title={"Modal Tambah Data Promosi"}
        >
          <>
           <TambahBannerModal 
           values={values} 
           ubahValue={handleChange}
           onCreate={handleSubmit}
           onUpdate={handleUpdate}
           handleCancel={resetData}
           mode={mode}
           />
         
        </>
        </ModalZapgo>
        <ModalZapgo 
            showModal={viewUpload}
            handleCancel={()=> setViewUpload(false)}
            title={"Modal Tambah Gambar Promosi"}
        >

        <TambahFoto simpanFoto={tambahFoto} handleFoto={(e) => setFile(e.target.files[0])} /> 
        </ModalZapgo>
      <TitleWithButton 
      handle
      onClick={() => setView(true)} isButton={true} title={"Master Data Promosi"}>
      <Search 
        placeholder="Cari" 
        allowClear 
        onChange={(e)=> setInitialData(e.target.value)}
        onSearch={() => setSearchKey(initial)} 
        size="large"
        style={{ width: 200 ,padding:"10px 5px" }} />
      </TitleWithButton>
          <div><TableCustomer
          editData={(record) => actiontambah(record)} hapus={(record)=>actionDelete(record)} hapusFoto={(record)=> hapusImage(record)} data={dataBanner && dataBanner}
          tambahFoto={(record)=> viewTambahFoto(record)}
          /></div>
      </ContentAndHeader>
    )
}
