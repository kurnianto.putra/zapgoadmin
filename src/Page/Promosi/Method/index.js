import {requestOnline, requestOnlineUploadFoto} from "../../../Utils/request"

export const tambahBanner = (data) => requestOnline("POST", `/banner` ,data)
export const listBanner = (limit=5, from=0 , cari = "") => requestOnline("GET", `/banner?limit=${limit}&from=${from}&cari=${cari}`)
export const deleteBanner = id => requestOnline("DELETE" , `/banner/${id}`)
export const updateBanner = (id,data) => requestOnline("PUT" , `/banner/${id}` , data)
export const uploadGambar = (data) => requestOnlineUploadFoto("POST", `/gambar`, data)
export const deleteGambar = (id) => requestOnlineUploadFoto("DELETE", `/gambar?uid_gambar=${id}`)
