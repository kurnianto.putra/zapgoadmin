import Style from "../Customer.module.css"
import "./Column.css"
import Icon , {DeleteOutlined , EditOutlined } from "@ant-design/icons"
import {Divider } from "antd"

const Columns = ({hapus , editData}) =>[
  {
    title: 'Tanggal Buat',
    dataIndex: 'create_at',
    key: 'create_at',
    render: text => <div className={Style.text}>{new Date(text).toLocaleDateString("id-ID")}</div>,

  },
  {
    title: 'Nama Negara',
    dataIndex: 'nama_negara',
    key: 'nama_negara',
    render: text => <div className={Style.text}>{text}</div>,
  },
  
  {
    title: 'Keterangan',
    dataIndex: 'keterangan',
    key: 'keterangan',
    render: text => <div className={Style.text}>{text}</div>,

  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: (_, record) => {
        return (
          <span style={{display:'flex'}}>
            <Icon onClick={()=> hapus(record)} component={DeleteOutlined} style={{ fontSize: '19px' , cursor:"pointer" }}/>
            <Divider type="vertical" />
            <Icon onClick={()=> editData(record)} component={EditOutlined} style={{ fontSize: '19px' , cursor:"pointer" }}/>
          </span>
        )
    }
  }
];


export default Columns;