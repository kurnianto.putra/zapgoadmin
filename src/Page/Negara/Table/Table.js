import { Table } from 'antd'
import React from 'react'
import Columns from "./Column"
export default function TableCustomer({data , hapus , editData}) {
    return (
        <Table
            dataSource={data}
            columns={Columns({hapus , editData})}
            pagination={{pageSize:5}}
            />
    )
}