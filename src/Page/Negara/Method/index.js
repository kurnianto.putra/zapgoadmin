import {requestOnline} from "../../../Utils/request"

export const tambahNegara = (data) => requestOnline("POST", `/negara` ,data)
export const listNegara = (cari = "") => requestOnline("GET", `/negara?cari=${cari}`)
export const deleteNegara = id => requestOnline("DELETE" , `/negara/${id}`)
export const updateNegara = (id,data) => requestOnline("PUT" , `/negara/${id}` , data)
