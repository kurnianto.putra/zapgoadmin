import React,{useState , useEffect} from 'react'
import ContentAndHeader from '../../Components/Reusable/Content'
import TableCustomer from './Table/Table'
import TitleWithButton from '../../Components/Reusable/TitleWithButtton'
import { Modal,  Input} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import useNotification from '../../Components/Reusable/Alert'
import TambahBannerModal from './Hooks/tambahData'
import useForm from '../../Components/Reusable/useForm'
import ModalZapgo from '../../Components/Reusable/Modal'
import { deleteNegara, tambahNegara , listNegara , updateNegara } from './Method'
const { Search } = Input;
const { confirm } = Modal;
export default function SlidePromosi() {
  const { values, handleChange, handleSubmit, setValues } =useForm(formBanner);
  const [view, setView] = useState(false)
  const [mode,setMode] = useState("create")
  const [counter, setCounter] = useState(0)
  const [dataNegara , setDataNegara] = useState([])
  const [key, setSearchKey] = useState("")
  const [initial , setInitialData] = useState("")
  const {alertShow} = useNotification()

  const getDataNegara = (key) => listNegara(key).then(({response})=> setDataNegara(response)) 
  
  useEffect(()=>{
    let isFecth = true 

    if(isFecth){
      getDataNegara(key)
    }
  },[counter, key])
  function actionDelete(data){
     confirm({
       title: 'Apakah Anda yakin untuk menghapus?',
       icon: <ExclamationCircleOutlined />,
       content: `Menekan tombol "Ya" berarti anda bersedia untuk menghapus data negara ${data.nama_negara} secara permanen`,
       okText: 'Yes',
       okType: 'danger',
       cancelText: 'No',
       onOk() {
         deleteNegara(data.uid)
         .then(()=> setCounter((count)=> count+1))
         .then(()=> alertShow("success",`Berhasil menghapus negara ${data.nama_negara}` ))
         .catch(()=> alertShow("error",`Gagal menghapus negara ${data.nama_negara}` ))
       },
       onCancel() {
         console.log('Cancel');
       },
     });
  }

  function formBanner(){
    const data = {
      nama_negara : values.nama_negara ,
      keterangan : values.keterangan
    }
    tambahNegara(data)
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil menyimpan negara ${data.nama_negara}`)
      setValues({})
      setView(false)
    }
    )
    .catch(()=> alertShow("error",`Gagal menyimpan negara ${data.nama_negara}` ))
  }


  function actiontambah(record){
    setView(true)
      setMode("edit");
      setValues({
        keterangan: record.keterangan,
        nama_negara: record.nama_negara,
        uid: record.uid
      })
  }

  function resetData(){
    setValues({})
    setView(false)
  }

  function handleUpdate(){
    const data = {
      nama_negara : values.nama_negara      
    }
    updateNegara(values.uid, data )
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil Update negara ${data.nama_negara}`)
      setValues({})
      setView(false)
    }
    )
    .catch(()=> alertShow("error",`Gagal Update negara ${data.nama_negara}` ))
  }


  return (
      <ContentAndHeader>
          <ModalZapgo 
            showModal={view}
            handleCancel={()=> setView(false)}
            title={"Modal Tambah Data Negara"}
        >
           <TambahBannerModal 
           values={values} 
           ubahValue={handleChange}
           onCreate={handleSubmit}
           onUpdate={handleUpdate}
           handleCancel={resetData}
           mode={mode}
           />
        </ModalZapgo>
      <TitleWithButton 
      handle
      onClick={() => setView(true)} isButton={true} title={"Master Data Negara"}>
      <Search 
        placeholder="Cari" 
        allowClear 
        onChange={(e)=> setInitialData(e.target.value)}
        onSearch={() => setSearchKey(initial)} 
        size="large"
        style={{ width: 200 ,padding:"10px 5px" }} />
      </TitleWithButton>
          <div>
            <TableCustomer
          editData={(record) => actiontambah(record)} hapus={(record)=>actionDelete(record)} data={dataNegara && dataNegara}
          
          /></div>
      </ContentAndHeader>
    )
}
