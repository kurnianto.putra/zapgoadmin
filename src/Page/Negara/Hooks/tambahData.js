import React from 'react'
import Style from "../../../Components/Reusable/TitleWithButtton/TitleWithButton.module.css"
import {Button} from "antd"
import InputAnd from '../../../Components/Reusable/InputAnd'
function TambahBannerModal({handleCancel, mode, onCreate , onUpdate , values, ubahValue}) {
        const simpanData = (e) => {
          e.preventDefault();
          mode === "create" ? onCreate() : onUpdate();
         }
    return (
   <>
          <InputAnd 
             type="text"
              panjang={"200"}
              placeholder="Nama Negara"
              name="nama_negara"
              value={values.nama_negara || ""}
              onChange={ubahValue}
              key={"nama_negara"}
              label={"Nama Negara"}

       
           
           />
          <InputAnd 
            name="keterangan"
            key={"keterangan"}
            value={values.keterangan || ""}
            onChange={ubahValue}
            placeholder={"Deskripsi"}
            label={"Deskripsi"}
            disabled={mode === "edit"}
          />
          <Button key="back"
          className={Style["buttonclose"]}
          onClick={handleCancel}>
          Return
        </Button>,
        <Button
          className={Style["buttonsave"]}
          key="submit" type="primary" onClick={(e) =>simpanData(e)}>
          Submit
        </Button>
    </>
    )
}
export default TambahBannerModal