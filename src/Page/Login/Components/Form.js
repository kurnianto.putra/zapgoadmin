import React, { useState , useContext  } from 'react';
import { Form, Input} from 'antd';
import { baseUrl } from "../../../Utils/api"
import {useNavigate , Navigate} from "react-router-dom"
import axios from "axios";
import { AuthContext } from "../../../Utils/context";
import { notification } from "antd";
import Style from  "./Login.module.css"
export const Demo = () => {

  const { setLoggedIn } = useContext(AuthContext);
  let navigate = useNavigate();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const onSubmit = () => {
    let authInfo = {
      username: username,
      pwd:password
    }
    axios
      .post(`${baseUrl}/akun_user/login`, authInfo)
      .then(({data}) => {
        console.log("data", data)
        const {response} = data
          setLoggedIn(true);
          localStorage.setItem("toksen",response[0].token);
          localStorage.setItem("user", response[0].nama);
          navigate("/artikel")
      })
      .catch((err) => {
        console.log(err);
        notification["error"]({
          message: "Notification Login",
          description: "Username atau password salah!",
        });
      });
  };

  if (localStorage.getItem("toksen")) {
    return <Navigate to="/artikel" />;
  }

  return (
    <div className={Style.container}>
      <div className={Style.screen}>
        <div className={Style.logo}>
          {/* <div className={Style.zap}>
            <img src={process.env.PUBLIC_URL + "/img/logozap.svg"} alt="logo" />
          </div> */}
          <span>LOGIN ZAPGO</span>
        </div>
        <div className={Style.bottom}>
          <Form>
              <Input placeholder="Username" onChange={(e)=> setUsername(e.target.value)} />
              <Input.Password placeholder="Password"onChange={(e)=> setPassword(e.target.value)} style={{marginTop:"15px"}} />
              <button onClick={() => onSubmit()}>Login</button>
          </Form>
        </div>
      </div>
    </div>
    
  );
};