import React,{useState} from 'react'
import ContentAndHeader from '../../Components/Reusable/Content'
import useGetCustomer from './Hooks'
import TableCustomer from './Table/Table'
import locale from "antd/es/date-picker/locale/id_ID";
import TitleWithButton from '../../Components/Reusable/TitleWithButtton'
import { DownloadOutlined , ArrowRightOutlined, CalendarOutlined } from '@ant-design/icons'
import { Button } from 'antd'
import { Input } from 'antd'
import ModalZapgo from '../../Components/Reusable/Modal'
import { DatePicker } from 'antd';
import Style from "../../Components/Reusable/TitleWithButtton/TitleWithButton.module.css"
import { downloadFile } from './Method';
import InputAnd from '../../Components/Reusable/InputAnd';
import CustomSelect from '../../Components/Reusable/Select';
const { RangePicker } = DatePicker;

const { Search } = Input;
export default function Customer({name}) {
  const [initialData , setInitialData] = useState("")
  const [view, setView] = useState(false)
	const [keyValue, setKeyValue] = useState("");
  const [pencarian, setPencarian] = useState("nama_user")
  const [query, setQuery] = useState({
    nama_user: "",
    jenis_barang: "",
    no_hp: "",
  });
  const [loading, setLoading] = useState(false)
  const [selectedPeriode, setSelectedPeriode] = useState([]);
  const {data} = useGetCustomer(query)
  function onSearch(){
    setQuery(initialData)
  }  

  function downloadFiles(){
    setLoading(true)
    downloadFile(selectedPeriode[0], selectedPeriode[1])
    .then((response) => response.blob())
    .then((blob) => {
     
      // 2. Create blob link to download
      const url = window.URL.createObjectURL(new Blob([blob]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `order_from_${selectedPeriode[0]}_to_${selectedPeriode[1]}.xlsx`);
      // 3. Append to html page
      document.body.appendChild(link);
      // 4. Force download
      link.click();
      // 5. Clean up and remove the link
      link.parentNode.removeChild(link);
     
    }).finally(()=> setLoading(false))
  }
let isDisabled = !selectedPeriode[0] || !selectedPeriode[1]
  function resetData(){
    setKeyValue(new Date())
    setView(false)
  }

  function handleChange(val){
    setPencarian(val.value)
    setTimeout(() => {
      setQuery({    
        nama_user: "",
      jenis_barang: "",
      no_hp: "" });
    }, 100);
  }
  return (
      <ContentAndHeader>
          <ModalZapgo 
            showModal={view}
            handleCancel={()=> setView(false)}
            title={"Download Document"}
        >
          <div style={{padding:"10px"}}>
         
         <RangePicker
						picker="date"
						locale={locale}
						size="large"
            key={keyValue}
						format="DD MMMM yyyy"
						onChange={(e, x) => {
							setSelectedPeriode([e?.[0]?.format("yyyy-MM-DD") || "", e?.[1]?.format("yyyy-MM-DD") || ""]);
						}}
						separator={<ArrowRightOutlined />}
						suffixIcon={<CalendarOutlined /> }
						popupStyle={{ top: 0 }}
						placeholder={["Mulai dari", "Sampai"]}
					/>
          </div>
          <div>
            <Button key="back"
            className={Style["buttonclose"]}
            onClick={resetData} >
          Return
        </Button>
        <Button
          className={Style["buttonsave"]}
          disabled={isDisabled}
          loading={loading}
          key="submit" type="primary" onClick={(e) =>downloadFiles(e)}>
         Download
        </Button>
          </div>
        </ModalZapgo>
      <TitleWithButton title={"Master Data Customer"}>
        <CustomSelect handleChange={handleChange}/>
      <Search 
          placeholder={`Cari ${pencarian}`}  
          allowClear 
          size="large"
          onChange={(e)=> setInitialData(e.target.value)}
          onSearch={onSearch} 
          style={{ width: 200 ,padding:"10px 5px" }} />
    <Button size={"large"}
    onClick={()=> setView(true)}
    icon={<DownloadOutlined />}
    >Download Data</Button>
      </TitleWithButton>
          <div><TableCustomer data={data}/></div>
      </ContentAndHeader>
    )
}
