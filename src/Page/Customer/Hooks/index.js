import {useState, useEffect} from 'react'
import {requestOnline} from "../../../Utils/request"
export default function useGetCustomer(query) {
    console.log("quey", query)
    const [data,setData] = useState([])
    const getDataCust = async (query) => {
        await requestOnline("GET", `/order/?nama_user=${query}`).then(({response})=> setData(response)) 
    }
        useEffect(()=>{
            let isFetch = true
            if(isFetch)(
                getDataCust(query)
            )
            return () => isFetch = false
        },[query])
    return {data}

}
