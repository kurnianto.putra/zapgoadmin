import Style from "../Customer.module.css"
export const Columns = [
  {
    title: 'Tanggal Pemesanan',
    dataIndex: 'create_at',
    key: 'create_at',
    fixed: 'left',
    width:160,
    render: text => <div className={Style.text}>{new Date(text).toLocaleDateString("id-ID")}</div>,

  },
  {
    title: 'Nama Customer',
    dataIndex: 'nama_user',
    key: 'nama_user',
    fixed: 'left',
    width:200,
    render: text => <div className={Style.text}>{text}</div>,
  },
  {
    title: 'No Hp',
    dataIndex: 'no_hp',
    key: 'no_hp',
    width:190,
    render: text => <div className={Style.text}>{text}</div>,

  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
    width:200,
    render: text => <div className={Style.text}>{text}</div>,

  },
  {
    title: 'Jenis Barang',
    dataIndex: 'jenis_barang',
    key: 'jenis_barang',
    width:200,
    render: text => <div className={Style.text}>{text}</div>,

  },
  {
    title: 'Berat Barang',
    dataIndex: 'berat',
    key: 'berat',
    width:190,
    render: text => <div className={Style.text}>{text} kg</div>,

  },
  {
    title: 'Dimensi Barang',
    dataIndex: 'dimensi',
    key: 'dimensi',
    width:350,
    render: text => <div className={Style.wrapdimensi}>
      <span className={Style.Panjang}>P : {text[0]} cm</span>
      <span className={Style.Lebar}>L : {text[1]} cm</span>
      <span className={Style.Tinggi}>T : {text[2]} cm</span>
      
      </div>,

  },
  {
    title: 'Tujuan',
    dataIndex: 'tujuan',
    key: 'tujuan',
    render: text => <div className={Style.text}>{text}</div>,

  },
];