import { Table } from 'antd'
import React from 'react'
import {Columns} from "./Column"
export default function TableCustomer({data}) {
    return (
        <Table
            dataSource={data}
            columns={Columns}
            scroll={{ x: '1800px' }}
        />
    )
}
