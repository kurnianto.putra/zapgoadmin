import Style from "../Customer.module.css"
import "./Column.css"
import Icon , {DeleteOutlined , EditOutlined , UploadOutlined} from "@ant-design/icons"
import {Divider, Button } from "antd"

import { Image } from 'antd';
const Columns = ({hapus , tambahFoto, editData, hapusFoto}) =>[
  {
    title: 'Tanggal Buat',
    dataIndex: 'create_at',
    key: 'create_at',
    render: text => <div className={Style.text}>{new Date(text).toLocaleDateString("id-ID")}</div>,

  },
  {
    title: 'Nama Banner',
    dataIndex: 'nama_bg',
    key: 'nama_bg',
    render: text => <div className={Style.text}>{text}</div>,
  },
  {
    title: 'Alt Text',
    dataIndex: 'alt_text',
    key: 'alt_text',
    render: text => <div className={Style.text}>{text}</div>,
  },
  {
    title: 'Action',
    dataIndex: 'action',
    render: (_,record) => {
      if(record?.gambar.length >= 1){
        return <div className="wrapbuttonaksi"> 
          <div className="boxsatu">
            <Image src={record.gambar} width={50} height={50}/>
          </div>
          <div className="boxsatu">
           <Button onClick={()=> hapusFoto(record)} type="primary" shape="circle" icon={<DeleteOutlined/>}/>
         </div>
         </div>
      }
      return  <Button type="primary" onClick={() => tambahFoto(record)} icon={<UploadOutlined />} size={"large"}>
      Upload
    </Button>
  }
},
  {
    title: 'Keterangan',
    dataIndex: 'keterangan',
    key: 'keterangan',
    render: text => <div className={Style.text}>{text}</div>,

  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: (_, record) => {
        return (
          <span style={{display:'flex'}}>
            <Icon onClick={()=> hapus(record)} component={DeleteOutlined} style={{ fontSize: '19px' , cursor:"pointer" }}/>
            <Divider type="vertical" />
            <Icon onClick={()=> editData(record)} component={EditOutlined} style={{ fontSize: '19px' , cursor:"pointer" }}/>
          </span>
        )
    }
  }
];


export default Columns;