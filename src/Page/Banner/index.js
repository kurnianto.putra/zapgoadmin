import React,{useState , useEffect} from 'react'
import ContentAndHeader from '../../Components/Reusable/Content'
import TableCustomer from './Table/Table'
import TitleWithButton from '../../Components/Reusable/TitleWithButtton'
import { Modal,  Input} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import useNotification from '../../Components/Reusable/Alert'
import TambahBannerModal from './Hooks/tambahData'
import TambahFoto from './Hooks/tambahFoto'
import useForm from '../../Components/Reusable/useForm'
import ModalZapgo from '../../Components/Reusable/Modal'
import { deleteBackground, listBackground, tambahBackground , updateBackground } from './Method'
import { deleteGambar , uploadGambar } from '../Promosi/Method';
const { Search } = Input;
const { confirm } = Modal;
export default function SlidePromosi() {
  const { values, handleChange, handleSubmit, setValues } =useForm(formBanner);
  const [view, setView] = useState(false)
  const [mode,setMode] = useState("create")
  const [counter, setCounter] = useState(0)
  const [dataBanner , setDataBanner] = useState([])
  const [viewUpload, setViewUpload] = useState(false)
  const [key, setSearchKey] = useState("")
  const [initial , setInitialData] = useState("")
  const [file, setFile] = useState("")
  const {alertShow} = useNotification()

  const getDataCust = (key) => listBackground(5,0,key).then(({response})=> setDataBanner(response)) 
  
  useEffect(()=>{
    let isFecth = true 

    if(isFecth){
      getDataCust(key)
    }
  },[counter, key])
  function actionDelete(data){
     confirm({
       title: 'Apakah Anda yakin untuk menghapus?',
       icon: <ExclamationCircleOutlined />,
       content: `Menekan tombol "Ya" berarti anda bersedia untuk menghapus data banner ${data.nama_bg} secara permanen`,
       okText: 'Yes',
       okType: 'danger',
       cancelText: 'No',
       onOk() {
         deleteBackground(data.uid)
         .then(()=> setCounter((count)=> count+1))
         .then(()=> alertShow("success",`Berhasil menghapus banner ${data.nama_bg}` ))
         .catch(()=> alertShow("error",`Gagal menghapus banner ${data.nama_bg}` ))
       },
       onCancel() {
         console.log('Cancel');
       },
     });
  }

  function formBanner(){
    const data = {
      nama_bg : values.nama_bg ,
      keterangan : values.keterangan,
      alt_text : values.alt_text
    }
    tambahBackground(data)
    .then(()=> {
      setCounter(counter+1)
      alertShow("success",`Berhasil menyimpan banner ${data.nama_bg}`)
      setValues({})
      setView(false)
    }
    )
    .catch(()=> alertShow("error",`Gagal menyimpan banner ${data.nama_bg}` ))
  }


  function actiontambah(record){
    setView(true)
      setMode("edit");
      setValues({
        keterangan: record.keterangan,
        nama_bg: record.nama_bg,
        uid: record.uid,
        alt_text : record.alt_text
      })
  }

  function resetData(){
    setValues({})
    setView(false)
    setMode("create")
  }

  function handleUpdate(){
      const data = {
        nama_bg : values.nama_bg,
        alt_text : values.alt_text,
      }
      updateBackground(values.uid, data ) 
      .then(()=> {
        setCounter(counter+1)
        alertShow("success",`Berhasil Update banner ${data.nama_bg}`)
        setValues({})
        setView(false)
        setMode("create")
      }
      )
      .catch(()=> alertShow("error",`Gagal Update banner ${data.nama_bg}` ))
  
  }

  function viewTambahFoto(record){
    setViewUpload(true)
    setValues({
      keterangan: record.keterangan,
      nama_bg: record.nama_bg,
      uid: record.uid,
      alt_text : record.alt_text,

    })
  }
  function tambahFoto(){
    const form = new FormData();
    form.append("uid_gambar", values.uid);
    form.append("nama_gambar", file );
    form.append("kategori", values.nama_bg);
    uploadGambar(form)         
    .then(()=>{
      setCounter(counter+1)
      alertShow("success",`Berhasil  menambahkan gambar banner ${values.nama_bg}`)
      setValues({})
      setViewUpload(false)
    })
    .catch(()=> alertShow("error",`Gagal menambahkan gambar banner ${values.nama_bg}` ))
  }

  function hapusImage(rec){
    deleteGambar(rec.uid)
    .then(()=>{
      setCounter(counter+1)
      alertShow("success",`Berhasil  mengahpus gambar banner`)
      setValues({})
      setViewUpload(false)
    })
    .catch(()=> alertShow("error",`Gagal mengahpus gambar banner` ))
  }


  return (
      <ContentAndHeader>
          <ModalZapgo 
            showModal={view}
            handleCancel={()=> setView(false)}
            title={"Modal Tambah Data Banner"}
        >
           <TambahBannerModal 
           values={values} 
           ubahValue={handleChange}
           onCreate={handleSubmit}
           onUpdate={handleUpdate}
           handleCancel={resetData}
           mode={mode}
           />
        </ModalZapgo>
        <ModalZapgo 
            showModal={viewUpload}
            handleCancel={()=> setViewUpload(false)}
            title={"Modal Tambah Gambar Banner"}
        >

        <TambahFoto simpanFoto={tambahFoto} handleFoto={(e) => setFile(e.target.files[0])} /> 
        </ModalZapgo>
      <TitleWithButton 
      handle
      onClick={() => setView(true)} isButton={true} title={"Master Data Banner"}>
      <Search 
        placeholder="Cari" 
        allowClear 
        onChange={(e)=> setInitialData(e.target.value)}
        onSearch={() => setSearchKey(initial)} 
        size="large"
        style={{ width: 200 ,padding:"10px 5px" }} />
      </TitleWithButton>
          <div><TableCustomer
          editData={(record) => actiontambah(record)} hapus={(record)=>actionDelete(record)} hapusFoto={(record)=> hapusImage(record)} data={dataBanner && dataBanner}
          tambahFoto={(record)=> viewTambahFoto(record)}
          /></div>
      </ContentAndHeader>
    )
}
