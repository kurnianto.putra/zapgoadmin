import {requestOnline} from "../../../Utils/request"

export const tambahBackground = (data) => requestOnline("POST", `/bg` ,data)
export const listBackground = (limit=5, from=0 , cari = "") => requestOnline("GET", `/bg?limit=${limit}&from=${from}&cari=${cari}`)
export const deleteBackground = id => requestOnline("DELETE" , `/bg/${id}`)
export const updateBackground = (id,data) => requestOnline("PUT" , `/bg/${id}` , data)