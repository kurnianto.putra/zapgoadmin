import React from 'react'
import Input from '../../../Components/Reusable/Input'
import Style from "../../../Components/Reusable/TitleWithButtton/TitleWithButton.module.css"
import {Button} from "antd"
import InputAnd from '../../../Components/Reusable/InputAnd'
function TambahBannerModal({handleCancel, mode, onCreate , onUpdate , values, ubahValue}) {
  console.log("value", values)
        const simpanData = (e) => {
          e.preventDefault();
          mode === "create" ? onCreate() : onUpdate();
         }
    return (
   <>
    <InputAnd 
           key={"banner"}
           label={"Nama Banner"}
           placeholder={"Nama Banner"}
           name="nama_bg"
           value={values.nama_bg || ""}
           onChange={ubahValue}
           placeholder={"Banner"}
         />
       <InputAnd 
           key={"keterangan"}
           label={"Keterangan"}
           placeholder={"Keterangan"}
           name="keterangan"
           value={values.keterangan || ""}
           onChange={ubahValue}
           placeholder={"Banner"}
           disabled={mode === "edit" ? true : false}
         />
          <InputAnd 
           key={"alt_text"}
           label={"Alt Text"}
           placeholder={"Alt Text"}
           name="alt_text"
           value={values.alt_text || ""}
           onChange={ubahValue}
          placeholder={"Alt Text Gambar (SEO)"}
         />
          <Button key="back"
          className={Style["buttonclose"]}
          onClick={handleCancel}>
          Return
        </Button>,
        <Button
          className={Style["buttonsave"]}
          key="submit" type="primary" onClick={(e) =>simpanData(e)}>
          Submit
        </Button>
    </>
    )
}
export default TambahBannerModal