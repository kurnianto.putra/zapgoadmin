import React, { useState } from "react";
import {
  Routes,
  Route,
} from "react-router-dom";
import './App.css';
import { Layout } from 'antd';
import SideBar from "./Components/Reusable/Sidebar";
import { Demo } from "./Page/Login/Components/Form";
import { AuthContext } from "./Utils/context";
import Banner from "./Page/Banner";
import PrivateRoute from "./Utils/privateRoute";
import Negara from "./Page/Negara";
import Artikel from "./Page/Artikel";
import Customer from "./Page/Customer";
import SlidePromosi from "./Page/Promosi";
function App() {
  const [loggedIn, setLoggedIn] = useState(
    localStorage.getItem("toksen") ? true : false
  );
  console.log("hai hai" , )

  return (
      <Layout style={{height:"100vh"}}>
       {loggedIn && <SideBar/> }
        <AuthContext.Provider value={{ loggedIn, setLoggedIn }}>
        <Routes>
          <Route exact path="/" element={<Demo />} />
          <Route
            path="/banner"
            element={
              <PrivateRoute auth={loggedIn}>
                <Banner />
              </PrivateRoute>
            }
          />
           <Route
            path="/negara"
            element={
              <PrivateRoute auth={loggedIn}>
                <Negara />
              </PrivateRoute>
            }
          />
            <Route
            path="/artikel"
            element={
              <PrivateRoute auth={loggedIn}>
                <Artikel />
              </PrivateRoute>
            }
          />
             <Route
            path="/customer"
            element={
              <PrivateRoute auth={loggedIn}>
                <Customer />
              </PrivateRoute>
            }
          />
             <Route
            path="/sliderpromosi"
            element={
              <PrivateRoute auth={loggedIn}>
                <SlidePromosi />
              </PrivateRoute>
            }
          />
        </Routes>
        </AuthContext.Provider>
      </Layout>
  );
}

export default App;
