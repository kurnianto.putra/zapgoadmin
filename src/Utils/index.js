function addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
const getCurrentDate = (view = true) => {
    let d = new Date();
    const months = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Augustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ];
    const days = [
      "Minggu",
      "Senin",
      "Selasa",
      "Rabu",
      "Kamis",
      "Jum'at",
      "Sabtu",
    ];
    let tanggal = d.getDate();
    let hari = d.getDay();
    let bulan = d.getMonth();
    let tahun = d.getFullYear();
    let jam = addZero(d.getHours());
    let menit = addZero(d.getMinutes());
  
    return `${view ? `${days[hari]},` : ""} ${tanggal} ${
      months[bulan]
    } ${tahun} - Jam ${jam}:${menit}`;
  };
  export {getCurrentDate}