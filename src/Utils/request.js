import {baseUrl} from './api'
function responOnline(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = text && JSON.parse(text);
      return Promise.reject(error);
    }
    return data;
  });
}

const requestOnlineDownload = async (method, api) => {
  const token = localStorage.getItem("toksen");
  const isLoggedIn = !!token;
return await fetch(`${baseUrl}${api}`, {
  method,
  headers: {
    "Content-Type": "application/json; charset=UTF-8",
    "token": `${isLoggedIn ? token : {}}`
  }
})
};

const requestOnline = async (method, api, body) => {
    const token = localStorage.getItem("toksen");
    const isLoggedIn = !!token;
  return await fetch(`${baseUrl}${api}`, {
    method,
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json; charset=UTF-8",
      "token": `${isLoggedIn ? token : {}}`
    }
  }).then(responOnline);
};
const requestOnlineUploadFoto = async (method, api, body) => {
  const token = localStorage.getItem("toksen");
  const isLoggedIn = !!token;
return await fetch(`${baseUrl}${api}`, {
  method,
  body: body,
  headers: {
    "token": `${isLoggedIn ? token : {}}`
  }
}).then(responOnline);
};




export {requestOnline , requestOnlineUploadFoto,requestOnlineDownload}